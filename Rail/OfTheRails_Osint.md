# Off the Rails
----
### Category : Osint


## Mission
I'm in trouble, I'm on the run... My friend told me he would meet me underneath a rail bridge 2km from his house before we escape on a train but he forgot to tell me where he lived.

He took two day trips relatively close by and sent me two photos. I also know that late last year, there was a fire near the suburbs approximately 25km from where he lives.

He lives somewhere in the middle of those three points. Could you tell me the name of the road parallel to the bridge, and the latitude and longitude of the bridge?

P.S. The longitude and latitude should be to 3 decimal places (plus if necessary include the . and/or -). Each segement of the flag should be seperated by an underscore. The road name type should be the same as when you find it. The flag is also case insensitive.

Example: DUCTF{latitude_longitude_nameofroad_roadnametype}

Author: scsc

Attached files:

koala.jpg (sha256: 71633f105b88a45a1ed237f196bdaf8ef706ad2c7eca3571e0519411da8f346d)
on_track.jpg (sha256: 77acaa70a5b20c5a7dc6f555316492891aa278f995646d6a5cc7750312b0b38c)

Given: ![koala.jpg](../pic/koala.jpg)[koala.jpg](../pic/koala.jpg)
![on_track.jpg](../pic/on_track.jpg)[on_track.jpg](../pic/on_track.jpg)

##Analyses

### koala.jpg
- the picture has geo cordinates
 ![geo.png](../pic/geo.png)
- this is in Area of Melbourne


### on_track.jpg
- Google reverse image search ---> says typ of the brige is a "Trestle Bridge" 
![bridgegoogle.png](../pic/bridgegoogle.png)

### fire in suburbs last year 25km
 - Google <fire suburbs melbourne 2019>
 - one of the first finds are [https://www.abc.net.au/news/2019-12-30/victoria-braces-for-day-of-extreme-bushfire-danger/11831632](https://www.abc.net.au/news/2019-12-30/victoria-braces-for-day-of-extreme-bushfire-danger/11831632)
 - Talking about Mill park bushfires
 - we guess we have a second location


-----

## Put the analyses in a map

(we use Google Earth)


the koala.jpg + the MillPark Location
![millpark--koala.png](../pic/millpark--koala.png)[millpark--koala.png](../pic/millpark--koala.png)

now we're looking for a "Trestle Bridge" in the north or south of the line at google maps and found 3 of them in the south
![trestlebridge.png](../pic/trestlebridge.png)
[https://www.google.com/maps/search/Trestle+Bridge+Melbourne/@-38.0778237,144.9043559,10.19z?hl=en](https://www.google.com/maps/search/Trestle+Bridge+Melbourne/@-38.0778237,144.9043559,10.19z?hl=en)

a quick look on the Picture to the links and we get the third location
 
![puffingbilly.png](../pic/puffingbilly.png)[puffingbilly.png](../pic/puffingbilly.png)

the Home must be somewhere in this square it is appr. 25km from the fires last year (2019)

![middle.png](../pic/middle.png)[middle.png](../pic/middle.png)
- [https://earth.google.com/web/@-37.75226545,145.33062863,130.23383457a,10879.47836254d,35y,349.56238401h,0t,0r](https://earth.google.com/web/@-37.75226545,145.33062863,130.23383457a,10879.47836254d,35y,349.56238401h,0t,0r)


# lets finish

now you have 3 possible rail bridges in reach of 2 km
![railbridges.png](../pic/railbridges.png)
- [railbridges.png](../pic/railbridges.png)
- [https://earth.google.com/web/@-37.73594851,145.3537186,92.36040067a,15749.19730704d,35y,-0h,0t,0r](https://earth.google.com/web/@-37.73594851,145.3537186,92.36040067a,15749.19730704d,35y,-0h,0t,0r)


and of corse ([Murphys law](https://en.wikipedia.org/wiki/Murphy%27s_law) ) it was the third one.....

![flag.png](../pic/flag.png)

[DUCTF{-37.751_145.351_Creek_Rd}](https://www.google.com/maps/place/37%C2%B045'06.5%22S+145%C2%B021'05.5%22E/@-37.7518211,145.3515242,108m/data=!3m1!1e3!4m14!1m7!3m6!1s0x6ad646b5d2ba4df7:0x4045675218ccd90!2sMelbourne+VIC,+Australia!3b1!8m2!3d-37.8136276!4d144.9630576!3m5!1s0x0:0x0!7e2!8m2!3d-37.7518066!4d145.3515157)

# Thanks a lot @scsc we had a lot of fun with this challenge

## Team: Hacklabor; Time to solve 3-4 h

## [hacklabor.de](https://hacklabor.de)
![https://hacklabor.de/assets/img/logo/Logo_Large_black.svg](https://hacklabor.de/assets/img/logo/Logo_Large_black.svg)










