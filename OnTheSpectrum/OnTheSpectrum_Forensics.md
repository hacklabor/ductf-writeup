# On the spectrum
---
### Category : Forensics



## Mission

My friend has been sending me lots of WAV files, I think he is trying to communicate with me, what is the message he sent?

Author: scsc

Attached files: message_1.wav (sha256: 069dacbd6d6d5ed9c0228a6f94bbbec4086bcf70a4eb7a150f3be0e09862b5ed)

I downloaded pasted the wav into Sonic Visualiser and with "SHIFT + G" I added Spectogram-Layer to it and got:

 ![spec.jpg](../pic/spec.jpg)[spec.jpg](../pic/spec.jpg)

 The stuff between 400 Hz and 4000 Hz looked good, so I zoomed in but is was still hard to read. I reduced the Gain to -7,5 dB and the colour to "Black on White". So I got the flag:

 ![specflag.jpg](../pic/specflag.jpg)[spec.jpg](../pic/specflag.jpg)

Still hard to read maybe but flag is:

DUCTF{m4by3_n0t_s0_h1dd3n}
